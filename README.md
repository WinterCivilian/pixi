## Name
Pixi

## Description
This project is for the coding challenge provided by Seepia Games.

## Installation
1. Download the repo.
2. Open up in VS Code.
3. Go to vite-project.
4. Run with npm run dev.
5. Open http://localhost:5173/ in your browser.

## Usage
This is for demonstrative purposes only.

## Authors and acknowledgment
The character model was provided to me by Seepia games.


## How to play
The character is moved with arrow keys or with the mouse.
There is no win/end-state.